import functools
import numpy as np

def binedges_from_centers(centers):
  edges = np.empty(centers.size+1)
  edges[1:-1] = (centers[1:] + centers[:-1])/2
  edges[0] = centers[0] - (edges[1]-centers[0])
  edges[-1] = centers[-1] + (centers[-1] - edges[-2])
  return edges

def binreduce(array, indices, ufunc, axis=0, dtype=None, out=None):
  """ Efficiently computes [np.ufunc.reduce(array[start,stop]) for start,stop in zip(binedges_i[:-1],binedges_i[1:])].
      Nearly the same as ufunc.reduceat, but leaves out the remainder and handles indices[-1]==array.shape[axis]. Assumes sorted indices. """

  if indices[-1]==array.shape[axis]:
    return ufunc.reduceat(array, indices[:-1], axis=axis, dtype=dtype, out=out)
  else:
    idx = [slice(None)] * array.ndim
    idx[axis] = slice(0, -1)
    idx = tuple(idx)

    return ufunc.reduceat(array, indices, axis=axis, dtype=dtype, out=out)[idx]

def binmean(array, indices, axis=0):
  """ Efficiently computes [np.mean(array[start,stop]) for start,stop in zip(indices[:-1],indices[1:])]. """

  shape = [1,]*array.ndim
  shape[axis] = indices.size-1
  shape = tuple(shape)

  return binreduce(array, indices, np.add, axis=axis)/binreduce(np.ones(array.shape[axis]), indices, np.add).reshape(shape)

binmax = functools.partial(binreduce, ufunc=np.maximum)
binmin = functools.partial(binreduce, ufunc=np.minimum)

def multi_binreduce(*args, operation=binmean):
  """ Convenience function for plots. Instead of ax.plot(x_fine, y_fine) just do
        ax.plot(*multi_binreduce(x_fine, np.linspace(x_fine.min(),x_fine.max(),100), y_fine))
      or just
        ax.plot(*multi_binreduce(x_fine, 100, y_fine))
      to get more efficient plotting by showing averaged data (first option gives you more control).
      To use maximum instead of averaging:
        ax.plot(*multi_binreduce(x_fine, np.linspace(x_fine.min(),x_fine.max(),100), y_fine, operation=functools.partial(binreduce,ufunc=np.maximum)))
      This function also works for multidimensional data.
  """
  fine_axes = args[:-1][::2]
  coarse_axes = args[:-1][1::2]
  data = args[-1]
  output_axes = []

  for i, (fine_axis, coarse_axis) in enumerate(zip(fine_axes, coarse_axes)):
    if coarse_axis is None:
      output_axes.append(fine_axis)
      continue

    if type(coarse_axis)==int:
      pts = coarse_axis
      coarse_axis = np.linspace(fine_axis.min(), fine_axis.max(), pts)

    binedges = binedges_from_centers(coarse_axis.ravel())
    binedges_i = np.searchsorted(fine_axis.ravel(), binedges)
    data = operation(data, binedges_i, axis=i)
    output_axes.append(coarse_axis)

  output_axes = tuple(output_axes)
  return output_axes + (data,)
