Reduce operations on binned numpy arrays.

Installation:

```
pip3 install git+https://gitlab.com/leberwurscht/binreduce.git
```

Example usage:

```python
fine_x, fine_y = ..., ...

coarse_x = ...
binedges = binreduce.binedges_from_centers(coarse_x)
binedges_i = np.searchsorted(fine_x, binedges)

coarse_y_avg = binreduce.binmean(fine_y, binedges_i, axis=0)
coarse_y_min = binreduce.binreduce(fine_y, binedges_i, np.minimum, axis=0)
coarse_y_max = binreduce.binreduce(fine_y, binedges_i, np.maximum, axis=0)
```

This module also contains a convenience function `multi_binreduce` for more efficient plotting of huge arrays by showing averaged data:

```python
#ax.plot(x_fine, y_fine) # slow

ax.plot(*multi_binreduce(x_fine, np.linspace(x_fine.min(),x_fine.max(),100), y_fine)) # fast

#ax.plot(*multi_binreduce(x_fine, 100, y_fine)) # same but shorter

# for maximum instead of averaging:
#ax.plot(*multi_binreduce(x_fine, np.linspace(x_fine.min(),x_fine.max(),100), y_fine, operation=functools.partial(binreduce,ufunc=np.maximum)))
#ax.plot(*multi_binreduce(x_fine, np.linspace(x_fine.min(),x_fine.max(),100), y_fine, operation=binmax)) # same but shorter
```

It also works for multidimensional data, e.g. `x_fine.shape==(10000,), y_fine.shape==(20000,), z_fine.shape==(10000,20000)`:

```python
x,y,z = multi_binreduce(x_fine, 150, y_fine, 200, z_fine)
# results in  x.shape==(150,), y.shape==(200,), y.shape==(150,200)
```
